# zabbix-cleaner
Zabbix MySQL database old data cleaner

Usage:
 mysql zabbix < zabbix-partitioning.sql
 mysql> CALL partition_maintenance_all('zabbix');

crontab:
 0 12 * * 1-5    echo "CALL partition_maintenance_all_day('zabbix');" | mysql -A zabbix

